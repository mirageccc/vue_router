//声明获取购物车的方法

export const fetchProduct = ()=>
    fetch('api/data/index.json').then(body=>body.json())

    //1.请求发起时，地址是
    //'/api/data/index.json'
    //2.经过代理之后会变成
    //'http://chst.vip/api/data/index.json'
    //3.路径重写之后
     //'http://chst.vip/data/index.json'
