import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home/index.vue";
import Discover from "../views/Discover/index.vue";
//把路由注入到所有的子组件
Vue.use(VueRouter);
//定义路由配置 路由配置一般由 path和component组成

//配置路由的步骤

//1.构建视图组件

//2.配置routes 一个path映射一个component

//3.router-view 配置视口
const routes = [
  {
    path: "/home",
    name: 'home',
    component: Home,
  },
  {
    path: "/discover",
     component:Discover,
     //重定向
    //  redirect:'/discover/toplist',

    
     //children这个属性和routes基本一致
     //二级路径的最前面不要带斜杠/
     children: [
      {
        //具名路由
        name:'toplist',
        path: "toplist",
        // component:()=>import(/*webpackChunkName：'tolist' */'@/views/Discover/Toplist'),
        components: {
          toplist: ()=>import(/*webpackChunkName：'tolist' */'@/views/Discover/Toplist'),
          djradio:()=>import(/*webpackChunkName：'djradio' */'@/views/Discover/Djradio'), 

        }
      },
      {
        name:'djradio',
        path: "djradio",
        component:()=>import(/*webpackChunkName：'djradio' */'@/views/Discover/Djradio'),
      },
     ]
  },
  {
    path: "/friend",
    //路由懒加载（按需加载）
    //将这段代码进行切割，加快首屏的渲染速度
     component:()=>import(/*webpackChunkName:'friend' */'../views/Friend/index.vue')
  },
  {
    path: "/goods",
     component:()=>import(/*webpackChunkName:'friend' */'../views/Goods/index.vue')
  },
  {
    path: "/details/:id",//动态路由参数 params参数
    props:true,//路由解耦
     component:()=>import(/*webpackChunkName:'details' */'../views/Details/index.vue')
  },
  {
    path:"*",
    component: () => import(/*webpackChunkName:'404' */'../views/Page404/index.vue')
  },
  {
    path:"/count",
    component: () => import(/*webpackChunkName:'404' */'@/views/count')
  },
  {
    path:"/",
     redirect: '/axios'
  },
  {
    path:"/gouwuche",
    component: () => import(/*webpackChunkName:'gouwuche' */'&/views/gouwuche')
  },
  {
    path:"/axios",
    component: () => import(/*webpackChunkName:'gouwuche' */'&/views/AxiosUse')
  },

];

export default  new VueRouter({
    routes,
})

