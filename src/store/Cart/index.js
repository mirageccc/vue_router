//添加到购物车的核心思路

//点击产品数据的时候获取这条数据,再把这条数据添加到当前的carts这个数组中就行了

//1.点击产品上的'加入购物车按钮',获取当前这条数据
//2.拿到数据之后,查找到这条数据,在当前的action中定义方法,接收
//3.加入到carts
export default {
    namespaced: true,
    state: {
        tag: "购物车模块store",
        carts: []
    },
    mutations: {
        SET_CART(state, payload) {
            //获取数据的id
            let id = payload.id;
            //用这个id到carts数组中查找有没有这条数据

            //如果有就让这条数据的数量加1
            //如果没有,就创建一条数据,添加到carts

            let target = state.carts.find(item => item.id == id)

            if (!target) { //没有这条数据
                let obj = {
                    id,
                    title: payload.title,
                    price: payload.price,
                    count: 1
                }

                state.carts.push(obj)
            } else {
                target.count++
            }
        }
    },
    actions: {
        GET_PRODUCT(context, payload) {
            // console.log(payload)
            //让Product模块化store中的products数组中对应的数据,库存-1

            //通过Mutation将传入的数据,添加到当前的carts状态中

            context.commit('SET_CART', payload)

            //触发Product模块中的mutaion
            context.commit("Product/DECREMENT_INVENTORY", payload.id, { root: true })

        }
    },
    getters: {
        total(state) {
            return state.carts.reduce((total, item) => {
                return total += item.count * item.price
            }, 0).toFixed(2)
        }
    },
    // modules: {}
}