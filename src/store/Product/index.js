import {fetchProduct}  from '@/api/gouwuche';

export default {
    namespaced: true,
    state:{
        tag:'产品模块store',
        products:[]//获取产品的数据
    },
    //给了命名空间后，这些属性都会加上模块的名字
    mutations:{
        //设置购物车数据
        SET_PRODUCT(state,payload){
            console.log(state);
            state.products = payload;
        },
        //减少库存
        DECREMENT_INVENTORY(state,payload){
            //对比目标数据，找到目标数据，让库存-1
            // console.log('------------');
            console.log(payload);
            // payload.inventory--
            // console.log(7777);
            //通过id查找到目标数据
            let target =state.products.find(item=> item.id === payload)

            if(target){//如果有这条目标数据，库存-1
                target.inventory--
            }
        }
    },
    actions:{
        //获取购物车数据
       async FETCH_PRODUCT(context,payload){
        //请求产品数据
          let res = await fetchProduct();
          console.log(res);
          //让mutation修改products数据
          context.commit('SET_PRODUCT',res.data)
        }
    },
    getters: {
        
    },
    // modules:{},
}