import Vue from "vue"

import Vuex from "vuex"

import Cart from './Cart' //模块化store引入

import Product from './Product'
//引入插件
import timeTravel from './plugin/timeTravel'
//引入持久化存储的插件
import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)
//一个项目只能有一个Store实例
export default new Vuex.Store({
    strict: true,//严格模式
    // 一共6个核心属性
    //状态
    state: {
        user: "lbw",
        num: 666,
        products: [],
        msg: "那时一条神奇的天路哟"
    },
    //用于变更状态
    mutations: {
        // `更改vuex中状态的唯一方式就是提交mutation`
        // mutations中的属性,都是由一个type和一个handle处理函数组成
        // 接收state和payload(荷载)作为参数
        INCREMENT_NUM(state, payload) {
            console.log(payload)
            state.num = state.num + payload
        },
        DECREMENT_NUM(state, payload) {
            state.num = state.num - payload
        },
        //mutations只能做同步操作

        //为什么mutations只能做同步操作
        //这个是vuex的设计规则,调试工具只能监听到mutations中的变更
        //vuex设计者希望我们把异步操作分离,而不是将逻辑写在一块,导致难以维护
        SET_PRODUCT(state, payload) {
            console.log(state);
            state.products = payload;
        }
    },
    //用于异步操作
    actions: {
        //actions定义的时候也是由一个类型type对应一个处理函数
        //这个处理函数接收context和payload作为参数
        GET_PRODUCT(context, payload) {
            console.log(context)
            console.log(payload)
            fetch('http://chst.vip/data/index.json')
                .then(body => body.json())
                .then(res => {
                    console.log(res)
                    context.commit("SET_PRODUCT", res.data)

                    // context.state.products = res.data
                })
        }
    },
    //数据预处理(相当于vuex的计算属性)
    getters: {
        reverseMsg(state) {
           // state.user = 'pdd';//直接修改状态
            return state.msg.split("").reverse().join("")
        }
    },
    //模块化
    modules: {
        Cart,
        Product

    },
    //插件
    plugins:[timeTravel,createPersistedState({
        // 存储方式：localStorage、sessionStorage、cookies
              storage: window.localStorage,
        // 存储的 key 的key值
              key: "2204",
              paths:['products','user']
      })],
})