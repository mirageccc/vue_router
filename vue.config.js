const { defineConfig } = require("@vue/cli-service");
const path = require("path");
function resolve(paths){
  return path.resolve(paths)
}

console.log('当前的环境是:' + process.env.NODE_ENV)
console.log('当前请求的服务器地址:' + process.env.VUE_APP_BASEURL)
console.log('当前的版本:' + process.env.VUE_APP_APPVERSION)
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false,
  //打包文件中的js等文件的目录变成当前目录
  publicPath:'./',
  outputDir:process.env.VUE_APP_DIR,
  //配置开发服务,代理
  devServer:{
    port:9090,//项目端口
    proxy: {//代理
     "/api":{
      //目标地址
      target:"http://chst.vip",
      // 路径重写
      pathRewrite:{"^/api":""},
     }
    }
  },
  chainWebpack: (config)=>{//配置别名,合并webpack配置
    config.resolve.alias
        .set('&', resolve('src'))
        .set('assets',resolve('src/assets'))
        .set('components',resolve('src/components'))
        .set('layout',resolve('src/layout'))
        .set('base',resolve('src/base'))
        .set('static',resolve('src/static'))
         //设置打包hash
    config.output
    .filename(
      `assets/js/[name].${process.env.VUE_APP_VERSION}.${+new Date()}.js`
    )
    .chunkFilename(
      `assets/js/[name].${process.env.VUE_APP_VERSION}.${+new Date()}.js`
    )
    .end()
     // 如果filenameHashing设置为了false，可以通过这段代码给打包出的css文件增加hash值
    // config.plugin('extract-css').tap(args => [{
    //   filename: `assets/css/${process.env.VUE_APP_VERSION}.${+new Date()}.[name].css`,
    //   chunkFilename: `assets/css/${process.env.VUE_APP_VERSION}.${+new Date()}.[name].css`
    // }])
  }


});
